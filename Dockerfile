FROM python:3.7-alpine

RUN pip install lorem

ADD logsim.py /

USER 1000

CMD ["python","-u","logsim.py"]
